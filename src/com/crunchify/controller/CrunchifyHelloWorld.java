package com.crunchify.controller;
 
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
//import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.crunchify.dao.CountryDAO;
import com.crunchify.dao.CovidHospitalImpl;
import com.crunchify.model.Country;
import com.google.gson.JsonObject;
/*
 * author: Crunchify.com
 * 
 */
@Controller
@RequestMapping("/welcome")
public class CrunchifyHelloWorld {
	
	//@Autowired
	//private CountryDAO countryDao;
 
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		//List<Map<String, Object>> hospitalInfoList = new ArrayList<Map<String, Object>>();
		String hospitalInfoList = "";
		String covidStatStr = "";
		String covidRateStr = "";
		//ListIterator<Object> listItr = null;
		CovidHospitalImpl chosImpl = new CovidHospitalImpl();
		hospitalInfoList = chosImpl.getHospitalInfo();
		covidStatStr = chosImpl.getCovidStats();
		covidRateStr = chosImpl.getInfRate();

		String result2 = covidRateStr;
				
		model.addAttribute("result2", result2 );
		model.addAttribute("result1", covidStatStr);
		model.addAttribute("credit", hospitalInfoList);
		
		return "welcome";
	}
	
	
}
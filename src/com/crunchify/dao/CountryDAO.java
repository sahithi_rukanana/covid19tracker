package com.crunchify.dao;
import java.util.List;

import com.crunchify.model.Country;

public interface CountryDAO {
	public List<Country> countryList();
}

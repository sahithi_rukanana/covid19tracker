package com.crunchify.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.crunchify.model.Country;


public class CountryDAOImpl implements CountryDAO{

	private JdbcTemplate jdbcTemp;

	public CountryDAOImpl(DataSource dataSource) {
		jdbcTemp = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Country> countryList() {
		String sql = "SELECT * FROM country ORDER BY Country_Name";
		List<Country> list = jdbcTemp.query(sql, new RowMapper<Country>() {

			@Override
			public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
				Country country = new Country();
				country.setCountryId(rs.getInt("Country_Id"));
				country.setCountryName(rs.getString("Country_Name"));
				return country;
			}

		});

		return list;
	}

}




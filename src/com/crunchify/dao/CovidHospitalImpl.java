package com.crunchify.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;


public class CovidHospitalImpl {
	String url="jdbc:mysql://localhost:3306/covid_tracker";
	String username="root";
    String password="password";
    String driver = "com.mysql.cj.jdbc.Driver";
	
	public String getHospitalInfo() {
		//ArrayList<Object> hospitalInfo = new ArrayList<Object>();
		List<Map<String, Object>> hospitalInfo = new ArrayList<Map<String, Object>>();
		String hospitalInfoStr = "";
		try
	    {
	      // create our mysql database connection
	      Class.forName(driver);
	      Connection conn = DriverManager.getConnection(url,username,password);
	      String query = 
	      "SELECT h.Country_Id,h.Hospital_Name,h.Location,e.Bed_Count,e.Ventrilator_Count FROM "+
	      "covid_hospitals h INNER JOIN equipment_info e on e.Hospital_id = h.Hospital_Id ";
	      // create the java statement
	      Statement st = conn.createStatement();
	      // execute the query, and get a java resultset
	      ResultSet rs = st.executeQuery(query);
	      ResultSetMetaData md = rs.getMetaData();
	      int columns = md.getColumnCount();
	      
	      // iterate through the java resultset
	      while (rs.next())
	      {
	    	Map<String, Object> row = new HashMap<String, Object>(columns);
	    	for(int i = 1; i <= columns; ++i){
	            row.put(md.getColumnName(i), rs.getObject(i));
	        }
	    	hospitalInfo.add(row);
	        hospitalInfoStr = new Gson().toJson(hospitalInfo);
	    	
			
	        	  
	      }
	      //System.out.println(hospitalInfo);
	      
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception! ");
	      System.err.println(e.getMessage());
	    }
		System.out.println(hospitalInfo); 
		return hospitalInfoStr;
	  }
	
	public String getCovidStats() {
		List<Map<String, Object>> covidInfo = new ArrayList<Map<String, Object>>();
		String covidStatStr = "";
		try
	    {
	      // create our mysql database connection
	      Class.forName(driver);
	      Connection conn = DriverManager.getConnection(url,username,password);
	      String statQuery = "SELECT * FROM covidstats WHERE "
	    		      		+ "CountryCode IN ('DE','IN','US','TW','GB','JP','BD','AU','NL')";
	      // create the java statement
	      Statement st = conn.createStatement();
	      // execute the query, and get a java resultset
	      ResultSet rs = st.executeQuery(statQuery);
	      ResultSetMetaData md = rs.getMetaData();
	      int columns = md.getColumnCount();
	      
	      // iterate through the java resultset
	      while (rs.next())
	      {
	    	Map<String, Object> row = new HashMap<String, Object>(columns);
	    	for(int i = 1; i <= columns; ++i){
	            row.put(md.getColumnName(i), rs.getObject(i));
	        }
	    	covidInfo.add(row);
	    	covidStatStr = new Gson().toJson(covidInfo);
	    	
	      }
	      //System.out.println(hospitalInfo);
	      
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception! ");
	      System.err.println(e.getMessage());
	    }
		System.out.println(covidInfo); 
		return covidStatStr;
	  }
	
	
	

	public String getInfRate() {
		List<Map<String, Object>> covidRate = new ArrayList<Map<String, Object>>();
		String covidRateStr = "";
		try
	    {
	      // create our mysql database connection
	      Class.forName(driver);
	      Connection conn = DriverManager.getConnection(url,username,password);
	      String rateQuery = "SELECT Country_Name,Infection_Rate "
	      		+ "FROM covid_tracker.country_rate;";
	      // create the java statement
	      Statement st = conn.createStatement();
	      // execute the query, and get a java resultset
	      ResultSet rs = st.executeQuery(rateQuery);
	      ResultSetMetaData md = rs.getMetaData();
	      int columns = md.getColumnCount();
	      
	      // iterate through the java resultset
	      while (rs.next())
	      {
	    	Map<String, Object> row = new HashMap<String, Object>(columns);
	    	for(int i = 1; i <= columns; ++i){
	            row.put(md.getColumnName(i), rs.getObject(i));
	        }
	    	covidRate.add(row);
	    	covidRateStr = new Gson().toJson(covidRate);
	    	
	      }
	      //System.out.println(hospitalInfo);
	      
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception! ");
	      System.err.println(e.getMessage());
	    }
		System.out.println(covidRate); 
		return covidRateStr;
	  }
	
	

}

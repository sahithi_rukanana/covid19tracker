package com.crunchify.model;

import java.util.Date;

public class Country {
	private int countryId;
	private String countryName;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;
	
	/*
	 * public Country(int countryId, String countryName) { super(); this.countryId =
	 * countryId; this.countryName = countryName; }
	 */
	/*
	 * public Country(int countryId, String countryName, String createdBy, Date
	 * createdDate, String modifiedBy, Date modifiedDate) { super(); this.countryId
	 * = countryId; this.countryName = countryName; this.createdBy = createdBy;
	 * this.createdDate = createdDate; this.modifiedBy = modifiedBy;
	 * this.modifiedDate = modifiedDate; }
	 */

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	


	}


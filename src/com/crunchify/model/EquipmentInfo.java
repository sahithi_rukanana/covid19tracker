package com.crunchify.model;

import java.util.Date;

public class EquipmentInfo {
	private Integer hospitalId;
	private Integer bedCount;
	private Integer ventrilatorCount;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;
	public EquipmentInfo(Integer hospitalId, Integer bedCount, Integer ventrilatorCount, String createdBy,
			Date createdDate, String modifiedBy, Date modifiedDate) {
		super();
		this.hospitalId = hospitalId;
		this.bedCount = bedCount;
		this.ventrilatorCount = ventrilatorCount;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
	}
	public Integer getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Integer hospitalId) {
		this.hospitalId = hospitalId;
	}
	public Integer getBedCount() {
		return bedCount;
	}
	public void setBedCount(Integer bedCount) {
		this.bedCount = bedCount;
	}
	public Integer getVentrilatorCount() {
		return ventrilatorCount;
	}
	public void setVentrilatorCount(Integer ventrilatorCount) {
		this.ventrilatorCount = ventrilatorCount;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	

}

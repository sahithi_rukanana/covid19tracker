<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<!-- let's add tag srping:url -->
<spring:url value="/resources/crunchify.css" var="crunchifyCSS" />
<spring:url value="/resources/crunchify.js" var="crunchifyJS" />
<spring:url value="/resources/tabulator.css" var="tabCSS" />
<spring:url value="/resources/tabulator.js" var="tabJS" />
<spring:url value="/resources/tabulator.min.css" var="tabminCSS" />
<spring:url value="/resources/tabulator.min.js" var="tabminJS" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href="${crunchifyCSS}" rel="stylesheet" />
<script src="${crunchifyJS}"></script>
<link href="${tabCSS}" rel="stylesheet" />
<script src="${tabJS}"></script>
<link href="${tabminCSS}" rel="stylesheet" />
<script src="${tabminJS}"></script>
<!-- Finish adding tags -->

<title>COVID-19 Tracker</title>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var creditC = ${credit};
	
	
	//mediTable(creditC);
	//$("#example-table").tabulator();
	displayTable(creditC)
	/* const tabledatanew = creditC;
	
		const table = new Tabulator("#example-table", {
		  height: 180,
		  data: tabledatanew,
		  layout: "fitColumns",
		  columns: [{
		    title: "Name",
		    field: "Hospital_Name",
		    width: 150
		  }, {
		    title: "Bed_Count",
		    field: "Bed_Count",
		    width: 150
		  }, {
		    title: "Location",
		    field: "Location"
		  }, {
		    title: "Ventrilator_Count",
		    field: "Ventrilator_Count"
		  }, ],
		  rowClick: function(e, row) {
		    alert("Row " + row.getData().id + " Clicked!!!!");
		  },
		}); */
		//console.log(table.getData());
});

</script>
<style type="text/css">
body {
	background-image: url('https://cdn.crunchify.com/bg.png');
}
</style>
</head>
<body>${message}
	<h2 align="center">COVID-19 Tracker</h2>
	<div align="center">
	<form>
	  Country Name &nbsp;
	  <select id="country" name="country">
	    <option value="1" selected="selected" >Germany</option>
	    <option value="2">India</option>
	    <option value="3">United States of America</option>
	    <option value="4">United kingdom</option>
	    <option value="5">China</option>
	    <option value="6">Japan</option>
	    <option value="7">Bangladesh</option>
	    <option value="8">Australia</option>
	    <option value="9">Netherlands</option>
	  </select>
	  <br>
	  <br>
	  <input value="Submit" type="button" onclick="GetSelectedValue()">
	</form>
	<br>
	 <br>
	<h2>COVID-19 Statistics Table</h2>
<table id="result" style="width:60%">
  <tr>
    <th>Total Confirmed</th>
    <th>Total Deaths</th>
    <th>Total Recovered</th>
  </tr>
  <tr>
    <td id="totCon">0</td>
    <td id="totDet">0</td>
    <td id="totRec">0</td>
  </tr>
</table></div>
<br><br>
<div align="center">
	<h2>Medical Information</h2>
	<!-- <input type='button' onclick='mediTable(creditC)' 
    	value='Medical Information' /> -->
   
    <!-- <label for="comment2"></label> -->
    <div id="example-table"></div>
 
    <p id='showData'></p>
    <p id='hospitalData'></p>
    
    <!-- medicalInfo() <p id='msg'></p> -->
</div>	 
	<br>
	<br>
<%-- 	<form>
	<div align="center">
		<br>
		<h1>${heading}</h1>
		<h2>${result1}</h2>
		<br>
		<h3>${result2}</h3>
		
		<br> <br> ${credit}
		<!-- <input maxlength="500000" type="hidden" id="hospitaldata" 
		value= ${credit} /> -->
	</div>
	</form> --%>
	<!--
	<div
		style="font-family: verdana; padding: 10px; border-radius: 10px; font-size: 12px; text-align: center;">
 		
 		 <h2>Checkout this font color. Loaded from 'crunchify.css' file under '/WebContent/go/' folder</h2> -->
 		<div align="center"  id="crunchifyMessage"></div>
 		<div align="center" id="updatedTime"><h4>Last Updated at</h4><h4 id="updatedTime"></h4></div>
		<!-- <br>  Spring MCV Tutorial by <a href="https://crunchify.com">Crunchify</a>.
		Click <a href="https://crunchify.com/category/java-tutorials/"
			target="_blank">here</a> for all Java and <a
			href='https://crunchify.com/category/spring-mvc/' target='_blank'>here</a>
		for all Spring MVC, Web Development examples.<br>
	</div> -->

	

<!--   <input type="submit" value="Save" id="savebtn" /> -->
</body>
</html>